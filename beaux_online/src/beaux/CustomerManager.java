package beaux;

/*
 * Author: Tlangelani Celeste Maluleke
 * Date: 25 February 2015
 * 
 */
import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import beaux.customer;
/*
 * Author: Tlangelani Celeste Maluleke
 * Date: 25 February 2015
 * 
 */
public class CustomerManager {

	public static void main(String[] args) {
		// loads configuration and creates a session factory
		Configuration configuration = new Configuration().configure();
		ServiceRegistryBuilder registry = new ServiceRegistryBuilder();
		registry.applySettings(configuration.getProperties());
		ServiceRegistry serviceRegistry = registry.buildServiceRegistry();
		SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		// opens a new session from the session factory
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		// persists two new Contact objects
		customer customer1 = new customer("celeste","maluleke","female","celeste@gmail.com","817717451","1875a","mole","naledi","1868");
		session.persist(customer1);
		customer customer2 = new customer("marcia","ramashilabele","female","marcia@gmail.com","825608875","185a","suzanne","witbank","1898");
		Serializable id = session.save(customer2);
		System.out.println("created id: " + id);
		/*
		// loads a new object from database
		Contact contact3 = (Contact) session.get(Contact.class, new Integer(1));
		if (contact3 == null) {
		System.out.println("There is no Contact object with id=1");
	 	} else {
		System.out.println("Contact3's name: " + contact3.getName());
		}
		
		// loads an object which is assumed exists
		Contact contact4 = (Contact) session.load(Contact.class, new Integer(4));
		System.out.println("Contact4's name: " + contact4.getName());
		
		// updates a loaded instance of a Contact object
		Contact contact5 = (Contact) session.load(Contact.class, new Integer(5));
		contact5.setEmail("info1atcompany.com");
		contact5.setTelephone("1234567890");
		session.update(contact5);
		// updates a detached instance of a Contact object
		Contact contact6 = new Contact(3, "Jobs", "jobsatapplet.com", "Cupertino", "0123456789");
		session.update(contact6);
		// deletes an object
		Contact contact7 = new Contact();
		contact7.setId(7);
		session.delete(contact7);
		// deletes a loaded instance of an object
		Contact contact8 = (Contact) session.load(Contact.class, new Integer(8));
		session.delete(contact8);*/
		// commits the transaction and closes the session
		session.getTransaction().commit();
		session.close();
		}
	
}
