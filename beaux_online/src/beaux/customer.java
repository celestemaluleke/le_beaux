package beaux;

/*
 * Author: Tlangelani Celeste Maluleke
 * Date: 25 February 2015
 * 
 */
public class customer {

	//instance variables
	private int id;
	private String first_name;
	private String last_name;
	private String gender;
	private String email;
	private String contact;
	private String house_no;
	private String street;
	private String location;
	private String zip_code;

	//constructor with no parameters
	public customer() {
	}

	//constructor with parameters
	public customer(int id, String first_name, String last_name, String gender, String email, String contact,
			String house_no, String street, String location, String zip_code) {

		this.id = id;
		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.email = email;
		this.contact = contact;
		this.house_no = house_no;
		this.street = street;
		this.location = location;
		this.zip_code = zip_code;
	}

	public customer(String first_name, String last_name, String gender, String email, String contact, String house_no,
			String street, String location, String zip_code) {

		this.first_name = first_name;
		this.last_name = last_name;
		this.gender = gender;
		this.email = email;
		this.contact = contact;
		this.house_no = house_no;
		this.street = street;
		this.location = location;
		this.zip_code = zip_code;
	}

	//get and set methods
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String lname) {
		this.last_name = lname;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
		;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getHouse_no() {
		return house_no;
	}

	public void setHouse_no(String house_no) {
		this.house_no = house_no;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getlocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getZip_code() {
		return zip_code;
	}

	public void setZip_code(String zip_code) {
		this.zip_code = zip_code;
	}

}
