package com.jwt.hibernate.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
 
import com.jwt.hibernate.bean.Customer;

public class CustomerDAO {
	
	public void addCustomerDetails(String first_name,String last_name,  String gender, 
			String house_no, String street_name,
			String surburb, String zip_code, String city, String contact_no, String dob, String email,
			String user_name,String password){
		   try {
			   
			// 1. configuring hibernate
	            Configuration configuration = new Configuration().configure();
	 
	            // 2. create sessionfactory
	            SessionFactory sessionFactory = configuration.buildSessionFactory();
	 
	            // 3. Get Session object
	            Session session = sessionFactory.openSession();
	 
	            // 4. Starting Transaction
	            Transaction transaction = session.beginTransaction();
	            Customer cus = new Customer();
	            cus.setfirst_name(first_name);
	            cus.setlast_name(last_name);
	            cus.setgender(gender);
	            cus.sethouse_no(house_no);
	            cus.setstreet_name(street_name);
	            cus.setsurburb(surburb);
	            cus.setzip_code(zip_code);
	            cus.setcity(city);
	            cus.setcontact_no(contact_no);
	            cus.setdob(dob);
	            cus.setemail(email);
	            cus.setuser_name(user_name);
	            cus.setPassword(password);
	            session.save(cus);
	            transaction.commit();
	            System.out.println("\n\n Details Added \n"); 
		   }catch (HibernateException e) {
	            System.out.println(e.getMessage());
	            System.out.println("error");
	        }
		
	}

}
