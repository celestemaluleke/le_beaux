package com.jwt.hibernate.bean;

public class Customer {

	private int ID;
	private String first_name;
	private String last_name;
	private String gender;
	private String house_no;
	private String street_name;
	private String surburb;
	private String zip_code;
	private String city;
	private String contact_no;
	private String dob;
	private String email;
	private String user_name;
	private String password;

public Customer(){
	
}

	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}

	public String getfirst_name() {
		return first_name;
	}

	public void setfirst_name(String last_name) {
		this.first_name = last_name;
	}

	public String getlast_name() {
		return last_name;
	}

	public void setlast_name(String first_name) {
		this.last_name = first_name;
	}

	public String getgender() {
		return gender;
	}

	public void setgender(String gender) {
		this.gender = gender;
	}

	public String gethouse_no() {
		return house_no;
	}

	public void sethouse_no(String house_no) {
		this.house_no = house_no;
	}

	public String getstreet_name() {
		return street_name;
	}

	public void setstreet_name(String street_name) {
		this.street_name = street_name;
	}

	public String getsurburb() {
		return surburb;
	}

	public void setsurburb(String surburb) {
		this.surburb = surburb;
	}

	public String getzip_code() {
		return zip_code;
	}

	public void setzip_code(String zip_code) {
		this.zip_code = zip_code;
	}

	public String getcity() {
		return city;
	}

	public void setcity(String city) {
		this.city = city;
	}

	public String getcontact_no() {
		return contact_no;
	}

	public void setcontact_no(String contact_no) {
		this.contact_no = contact_no;
	}

	public String getdob() {
		return dob;
	}

	public void setdob(String dob) {
		this.dob = dob;
	}

	public String getemail() {
		return email;
	}

	public void setemail(String email) {
		this.email = email;
	}

	public String getuser_name() {
		return user_name;
	}

	public void setuser_name(String user_name) {
		this.user_name = user_name;
	}
	

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
