package com.jwt.hibernate.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
 
import com.jwt.hibernate.dao.CustomerDAO;

public class UserControllerServlet extends HttpServlet {
	 private static final long serialVersionUID = 1L;
	 

	    protected void doPost(HttpServletRequest request,
	            HttpServletResponse response) throws ServletException, IOException {
	    	
	    	String first_name=request.getParameter("first_name");
	    	String last_name=request.getParameter("last_name");
	    	String gender=request.getParameter("gender");
	    	String house_no=request.getParameter("house_no");
	    	String street_name=request.getParameter("street_name");
	    	String surburb=request.getParameter("surburb");
	    	String zip_code=request.getParameter("zip_code");
	    	String city=request.getParameter("city");
	    	String contact_no=request.getParameter("contact_no");
	    	String dob=request.getParameter("dob");
	    	String email=request.getParameter("email");
	    	String user_name=request.getParameter("user_name");
	    	String password=request.getParameter("password");
	    	
	 
	      
	 
	        HttpSession session = request.getSession(true);
	        
	    
	        try {
	        CustomerDAO cusDAO = new CustomerDAO();
	        cusDAO.addCustomerDetails(first_name, last_name, gender, house_no, street_name, 
	        		surburb, zip_code, city, contact_no, dob, email, user_name, password );
	        response.sendRedirect("Success");
	        } catch (Exception e) {
	        e.printStackTrace();
	        }

}
}