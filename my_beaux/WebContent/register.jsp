<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration Form</title>
</head>
<body>
	<h1>Registration Form</h1>
	<form action="register" method="post">
		<table cellpadding="3pt">
			<tr>
				<td>First Name :</td>
				<td><input type="text" name="first_name" size="30" /></td>
			</tr>
			<tr>
				<td>Last Name :</td>
				<td><input type="text" name="last_name" size="30" /></td>
			</tr>
			<tr>
				<td>Gender :</td>
				<td><input type="text" name="gender" size="30" /></td>
			</tr>

			<tr>
				<td>House No :</td>
				<td><input type="text" name="house_no" size="30" /></td>
			</tr>
			<tr>
				<td>Street :</td>
				<td><input type="text" name="street_name" size="30" /></td>
			</tr>
			<tr>
				<td>surburb :</td>
				<td><input type="text" name="surburb" size="30" /></td>
			</tr>

			<tr>
				<td>Zip Code :</td>
				<td><input type="text" name="zip_code" size="30" /></td>
			</tr>
			<tr>
				<td>City :</td>
				<td><input type="text" name="city" size="30" /></td>
			</tr>
			<tr>
				<td>Contact Number :</td>
				<td><input type="text" name="contact_no" size="30" /></td>
			</tr>
			<tr>
				<td>Date Of Birth :</td>
				<td><input type="text" name="dob" size="30" /></td>
			</tr>
			<tr>
				<td>Email :</td>
				<td><input type="text" name="email" size="30" /></td>
			</tr>
			<tr>
				<td>User Name :</td>
				<td><input type="text" name="user_name" size="30" /></td>
			</tr>
			<tr>
				<td>Password:</td>
				<td><input type="password" name="password" size="30" /></td>
			</tr>

			<tr>
				<td>Re Enter Password :</td>
				<td><input type="password" name="password1" size="30" /></td>
			</tr>


		</table>

		<p />
		<input type="submit" value="Register" />
	</form>
</body>
</html>
